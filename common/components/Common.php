<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 21/12/16
 * Time: 13:52
 */

namespace dkit\banner\common\components;
use yii\base\BaseObject;
use Yii;

class Common extends BaseObject
{

    public static function normalizeSizeArray ($sizes = false)
    {
        $output = [];
        $array = $sizes ? $sizes : \Yii::$app->getModule('banner')->bannerSize;

        foreach ($array as $key => $value) {
            $output[$key] = $key.' '.implode('x', $value);
        }
        return $output;
    }

    public static function getCorrectArrayLanguagesForSelectize(){
        $correctLanguages = [];
        foreach(Yii::$app->urlManager->languages as $key => $value){
            $correctLanguages[] = [
                'short'  => $key,
                'locale' => $value,
            ];
        }

        return $correctLanguages;
    }

    public static function getLang(){
        if (strlen(Yii::$app->language) === 5) {
            return $lang = explode('-', Yii::$app->language)[0];
        } elseif (strlen(Yii::$app->language) === 2) {
            return $lang = Yii::$app->language;
        } else {
            throw new InvalidParamException(Yii::t(
                'event',
                'Can\'t assign a value to a $lang parameter, application language seems to be not well configured.'
            ));
        }
    }
}