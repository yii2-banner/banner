<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 24/03/16
 * Time: 15:48
 */

namespace dkit\banner\common\components;

use Imagine\Image\ImageInterface;
use Yii;
use yii\imagine\Image;

class ImageComponent
{

    public static $sizes = [
        'xs' => ['width' => 100, 'height' => 100,],
        'sm' => ['width' => 160, 'height' => 110,],
        'md' => ['width' => 300, 'height' => 200,],
        'ld' => ['width' => 850],
        'main' => ['width' => 240, 'height' => 240],
        'benchmark' => ['width' => 540, 'height' => 400],
        'sponsors' => ['width' => 125, 'height' => 50],
        'events' => ['width' => 200, 'height' => 200],
        'journal' => ['width' => 200, 'height' => 285],
        'horizontal 1' => ['width' => 560, 'height' => 80],
        'horizontal 2' => ['width' => 850, 'height' => 120],
    ];


    public static function thumbnail($img, $size)
    {

        $imgInfo = pathinfo($img);
        $outputImg = $imgInfo['filename'] . '-' . $size . '.' . $imgInfo['extension'];

        $outputImgPath = Yii::getAlias('@thumbnail') . '/' . $outputImg;
        $outputImgUrl = Yii::getAlias('@thumbnailUrl') . '/' . $outputImg;

        if (!file_exists($outputImgPath) && file_exists($img)) {
            Image::thumbnail($img, self::$sizes[$size]['width'], self::$sizes[$size]['height'],
                ImageInterface::THUMBNAIL_INSET)
                ->save($outputImgPath, ['jpeg_quality' => 90,'jpg_quality' => 90,'png_quality' => 90,]);
        }

        return $outputImgUrl;
    }


}