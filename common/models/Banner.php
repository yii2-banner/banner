<?php

namespace dkit\banner\common\models;

use Yii;

/**
 * This is the model class for table "banner".
 *
 * @property integer $id
 * @property string $type
 * @property string $img
 * @property string $url
 * @property string $date_start
 * @property string $date_end
 * @property integer $clicks
 * @property string $languages_enabled
 */
class Banner extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'banner';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['img', 'url', 'date_start', 'date_end', 'languages_enabled'], 'required'],
            [['date_start', 'date_end'], 'safe'],
            [['clicks', 'order'], 'integer'],
            [['type'], 'string', 'max' => 55],
            [['img', 'url'], 'string', 'max' => 255],
            [['languages_enabled'], 'string', 'max' => 11],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('banner', 'ID'),
            'type' => Yii::t('banner', 'Type'),
            'img' => Yii::t('banner', 'Img'),
            'url' => Yii::t('banner', 'Url'),
            'date_start' => Yii::t('banner', 'Date Start'),
            'date_end' => Yii::t('banner', 'Date End'),
            'clicks' => Yii::t('banner', 'Clicks'),
            'languages_enabled' => Yii::t('banner', 'Languages Enabled'),
            'order' => Yii::t('banner', 'Order'),
        ];
    }

    /**
     * @inheritdoc
     * @return BannerQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BannerQuery(get_called_class());
    }
}
