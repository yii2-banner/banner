<?php

namespace dkit\banner\common\models;

use dkit\banner\common\components\Common;

/**
 * This is the ActiveQuery class for [[Banner]].
 *
 * @see Banner
 */
class BannerQuery extends \yii\db\ActiveQuery
{
    
    /**
     * @inheritdoc
     * @return Banner[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Banner|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * Set [[andWhere]] query so that sections are fetched only for current language.
     * @return $this
     */
    public function localized()
    {
        return $this->andWhere(['like', 'banner.languages_enabled', Common::getLang()]);
    }

    public function showPeriod()
    {
        return $this->andWhere(['<=', 'banner.date_start', date('Y-m-d H:i:s')])
            ->andWhere(['>=', 'banner.date_end', date('Y-m-d H:i:s')]);
    }

    /**
     * Set [[orderBy]] query so that sections are ordered by `nav_sort` field.
     * @return $this
     */
    public function ordered()
    {
        return $this->orderBy(['banner.order' => SORT_ASC]);
    }
}
