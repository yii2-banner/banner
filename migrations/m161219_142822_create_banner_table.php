<?php

use yii\db\Migration;

/**
 * Handles the creation of table `banner`.
 */
class m161219_142822_create_banner_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%banner}}', [
            'id'            => $this->primaryKey(),
            'type'          => $this->string(55)->notNull()->defaultValue('sponsors'),
            'img'           => $this->string(255)->notNull(),
            'url'           => $this->string(255)->notNull(),
            'date_start'    => $this->dateTime()->notNull(),
            'date_end'      => $this->dateTime()->notNull(),
            'clicks'        => $this->integer()->notNull()->defaultValue(0),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%banner}}');
    }
}
