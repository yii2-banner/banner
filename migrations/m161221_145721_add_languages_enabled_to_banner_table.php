<?php

use yii\db\Migration;

class m161221_145721_add_languages_enabled_to_banner_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%banner}}', 'languages_enabled', $this->string(11)->notNull());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%banner}}', 'languages_enabled');
    }
}
