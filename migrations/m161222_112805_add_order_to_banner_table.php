<?php

use yii\db\Migration;

class m161222_112805_add_order_to_banner_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%banner}}', 'order', $this->integer(11)->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%banner}}', 'order');
    }
}
