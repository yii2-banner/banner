<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model dkit\banner\common\models\Banner */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="banner-form">

    <div class="row">
        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'languages_enabled', ['options' => ['class' => 'col-sm-4']])->widget(\dosamigos\selectize\SelectizeTextInput::className(), [
            'clientOptions' => [
                'valueField' => 'short',
                'labelField' => 'short',
                'searchField' => ['short', 'locale'],
                'plugins' => ['remove_button'],
                'maxItems' => 10,
                'delimiter' => ',',
                'options' => \dkit\banner\common\components\Common::getCorrectArrayLanguagesForSelectize(),
                'create' => false,
            ]
        ]); ?>
        <?= $form->field($model, 'type', ['options' => ['class' => 'col-sm-4']])->dropDownList(\dkit\banner\common\components\Common::normalizeSizeArray()); ?>
        <?= $form->field($model, 'order', ['options' => ['class' => 'col-sm-4']])->textInput() ; ?>
    </div>
    <div class="row">
        <?= $form->field($model, 'img', ['options' => ['class' => 'col-sm-6']])->widget(\ict\posts\common\components\ElFinderInput::className()) ?>

        <?= $form->field($model, 'url', ['options' => ['class' => 'col-sm-6']])->textInput(['maxlength' => true]) ?>
    </div>
    <div class="row">
        <?= $form->field($model, 'date_start', ['options' => ['class' => 'col-sm-6']])->widget(DateTimePicker::className(), [
            'options' => ['placeholder' => Yii::t('app', 'Select start time to show')],
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd hh:ii',
                'autoclose' => true,
                'calendarWeeks' => true,
                'todayHighlight' => true,
                'todayBtn' => true,
                'weekStart' => 1,
            ]
        ]);  ?>

        <?= $form->field($model, 'date_end', ['options' => ['class' => 'col-sm-6']])->widget(DateTimePicker::className(), [
            'options' => ['placeholder' => Yii::t('app', 'Select end of banner show')],
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd hh:ii',
                'autoclose' => true,
                'calendarWeeks' => true,
                'todayHighlight' => true,
                'todayBtn' => true,
                'weekStart' => 1,
            ]
        ]);  ?>
    </div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
